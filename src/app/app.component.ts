import { Component, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthService } from './services/auth.service';
import { BlockUIService } from './services/block-ui.service';
import { UserRestService } from './services/user-rest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
}) 
export class AppComponent implements OnInit {
  @ViewChild('sidenav', {static: true}) sidenav;
  @ViewChild('blockUI', {static: true}) blockUI;
  title = 'On-Time';
  user;
  pageRoute;


  constructor(private storage: LocalStorageService, private router: Router, private auth: AuthService, private blockUIService: BlockUIService, private userRestService: UserRestService) {}

  ngOnInit() {
    var _self = this
    setTimeout(() => {
      this.router.events
        .subscribe(event => {
            _self.pageRoute = _self.router.url.split('/')[1]
        });
        _self.pageRoute = _self.router.url.split('/')[1]
    }, 500);
    // get user from auth service
    this.user = this.auth.getUser();

    // subscribe to future changes on user
    this.auth.user.subscribe(data => {
      this.user = data
    });
    
    this.blockUIService.init(this.blockUI);
  }

  // naveigate to given route and toggle sidebar
  navigateTo(to: [any]) {
    // this.sidenav.toggle();
    this.pageRoute = to
    this.router.navigate(to);
  }

  // perform logout and toggle sidebar
  logout() {
    let user = this.storage.get('user')
    this.userRestService.logout(user)
      .subscribe( data => {
        console.log(data)
        });
    this.auth.logout();
    // this.sidenav.toggle();
  }

  navigateToHomePage() {
    this.router.navigate(['/home']);
  }

}

