import { NgModule, ErrorHandler } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserModule, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GestureConfig } from '@angular/material';
import { FormsModule } from '@angular/forms';

import { 
  PerfectScrollbarModule, 
  PERFECT_SCROLLBAR_CONFIG, 
  PerfectScrollbarConfigInterface
} from 'ngx-perfect-scrollbar';
import { LocalStorageModule } from 'angular-2-local-storage';
import { provideInterceptorService, InterceptorService } from 'ng2-interceptors';
import { XHRBackend, RequestOptions } from '@angular/http';

import { UserMediaDirective } from './directives/user-media.directive';
import { NgxFaceApiJsModule } from 'ngx-face-api-js';

// import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './shared/inmemory-db/inmemory-db.service';

import { routing } from './app.routing';
import { SharedModule } from './shared/shared.module';
import { SharedMaterialModule } from './shared/shared-material.module';


// services
import { httpInterceptor } from './services/http-interceptor.service';
import { AuthService } from './services/auth.service';
import { BlockUIService } from './services/block-ui.service';
import { UserRestService } from './services/user-rest.service';

// components
import { AppComponent } from './app.component';
import { RegisterComponent } from './views/register/register.component';
import { LoginComponent } from './views/login/login.component';
import { HomeComponent } from './views/home/home.component';
import { SmileComponent } from './views/smile/smile.component';
import { CameraSnapshotComponent } from './views/camera-snapshot/camera-snapshot.component';
import { ImagePickerComponent } from './views/image-picker/image-picker.component';
import { UpdateImageComponent } from './views/update-image/update-image.component';
import { UpdatePasswordComponent } from './views/update-password/update-password.component';
import { BlockUIComponent } from './views/block-ui/block-ui.component';

import { HttpClient, HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { ErrorHandlerService } from './shared/services/error-handler.service';
import { CommonFunctionsService } from './shared/services/commonFunctions.service';
import { ToastrModule } from 'ngx-toastr';
// import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { NgxPaginationModule } from 'ngx-pagination';

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

export function interceptorFactory(xhrBackend: XHRBackend, requestOptions: RequestOptions, authService, snackbar, blockUI){
  let service = new InterceptorService(xhrBackend, requestOptions);
  service.addInterceptor(new httpInterceptor(authService, snackbar, blockUI));
  return service;
}

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    SharedModule,
    SharedMaterialModule,
    HttpClientModule,
    HttpModule,
    PerfectScrollbarModule,
    NgxPaginationModule,
    ToastrModule.forRoot(), // ToastrModule added
    // NgxUiLoaderModule,
    LocalStorageModule.forRoot({
      prefix: 'faceRecognition',
      storageType: 'localStorage'
    }),
    NgxFaceApiJsModule.forRoot({
      modelsUrl: 'https://raw.githubusercontent.com/justadudewhohacks/face-api.js/master/weights',
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    InMemoryWebApiModule.forRoot(InMemoryDataService, { passThruUnknownUrl: true}),
    routing
  ],
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    HomeComponent,
    SmileComponent,
    CameraSnapshotComponent,
    ImagePickerComponent,
    UpdateImageComponent,
    UpdatePasswordComponent,
    UserMediaDirective,
    BlockUIComponent],
  providers: [
    AuthService,
    BlockUIService,
    UserRestService,
    {
      provide: InterceptorService,
      useFactory: interceptorFactory,
      deps: [XHRBackend, RequestOptions, AuthService, BlockUIService, UserRestService],
    },
    { provide: ErrorHandler, useClass: ErrorHandlerService },
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    { provide: PERFECT_SCROLLBAR_CONFIG, useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG },
    CommonFunctionsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }