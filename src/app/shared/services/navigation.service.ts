import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

interface IMenuItem {
  type: string; // Possible values: link/dropDown/icon/separator/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  tooltip?: string; // Tooltip text
  access?: string;
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
  badges?: IBadge[];
  component?: string;
  method?: string;
}
interface IChildItem {
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  access?: string;
  component?: string;
  method?: string;
  sub?: IChildItem[];
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

@Injectable()
export class NavigationService {
  constructor() {}
  iconMenu: IMenuItem[] = [
    // {
    //   name: "DASHBOARD",
    //   type: "dropDown",
    //   tooltip: "Dashboard",
    //   icon: "dashboard",
    //   state: "dashboard",
    //   sub: [
    //     { name: "Default", state: "default" },
    //     { name: "Analytics", state: "analytics" },
    //     { name: "Cryptocurrency", state: "crypto" },
    //     { name: "Dark Cards", state: "dark" }
    //   ]
    // },
    // {
    //   name: "DASHBOARD",
    //   type: "link",
    //   tooltip: "Dashboard",
    //   icon: "dashboard",
      
    //   access: 'member',
    //   state: "dashboard/default",
    //   component: 'dashboards', 
    //   method: 'GET'
    // },

    {
      name: "Digital Board",
      type: "link",
      tooltip: "Dashboard",
      icon: "assignment",
      state: "dashboard/list",
    },

    {
      name: "Dashboard",
      type: "link",
      tooltip: "Dashboard",
      icon: "dashboard",
      state: "dashboard/my-list",
      component: 'dashboard', 
      method: 'GET'
    },

{
      name: "Visualization",
      type: "link",
      tooltip: "Visualization",
      icon: "show_chart",
      access: 'member',
      state: "visualization/visualization-list",
      component: 'visualization',
      method: 'GET'
    },
    
    {
      name: "Data Source",
      type: "dropDown",
      tooltip: "Sources",
      icon: "sd_storage",
      state: "data-source",
      sub: [
        { name: "List", access: 'member', state: "list", component: 'data-source',  method: 'GET'}
        // { name: "Add Datasource", access: 'member', state: "new-datasorce" },
      ]
    },

    

    // {
    //   name: "Visualization-drill",
    //   type: "link",
    //   tooltip: "Dashboard",
    //   icon: "show_chart",
    //   access: 'member',
    //   state: "visualization-drill/visualization/diaj864587393b84-1e2d-3d54-60bf-689e92848704"
    // },

    {
      name: "Setup",
      type: "dropDown",
      tooltip: "Setup",
      icon: "settings",
      state: "set-up",
      sub: [
        { name: "Users", access: 'member', state: "user-list", component: 'users', method: 'GET'},
        { name: "Organisations", access: 'member', state: "organisation-list", component: 'organisation', method: 'GET'},
        { name: "Role", access: 'member', state: "role-management-list", component: 'role-management', method: 'GET'}
      ]
    }
  ];

  // Icon menu TITLE at the very top of navigation.
  // This title will appear if any icon type item is present in menu.
  iconTypeMenuTitle: string = "Frequently Accessed";
  // sets iconMenu as default;
  menuItems = new BehaviorSubject<IMenuItem[]>(this.iconMenu);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();

  // Customizer component uses this method to change menu.
  // You can remove this method and customizer component.
  // Or you can customize this method to supply different menu for
  // different user type.
  publishNavigationChange(menuType: string) {
    // switch (menuType) {
    //   case "separator-menu":
    //     this.menuItems.next(this.separatorMenu);
    //     break;
    //   case "icon-menu":
    //     this.menuItems.next(this.iconMenu);
    //     break;
    //   default:
    //     this.menuItems.next(this.plainMenu);
    // }
  }
}
