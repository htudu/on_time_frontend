import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Config } from '../config/general.config';
import { USER_ROLES } from '../config/env.config';
import { AuthService } from './auth.service';

import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

import { API_URL } from '../config/env.config';

@Injectable()
export class RoleGuard implements CanActivate {

  public role: string =  Config.getUserRole();
  public roleRequired:string = 'supadmin'
  userData:any;
  constructor(private router: Router,
    private _objService: AuthService,
    private httpClient: HttpClient) {
    
  }
  // Function to check if user is authorized to view route
  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

   if (this.role != 'supadmin' && this.role != 'admin') {
     
    return new Promise((resolve, reject) => {
    this._objService.getUserDataByID(Config.getUserId()).subscribe(userData => {
        this.userData = userData;
       this._objService.getRoleDataByOrgIdAndRoleName(userData.org_id, userData.usr_role).subscribe(roleData => {
        let roleInfo = roleData.components.find(item => item.component_name === router.data.component);
        switch (router.data.method){
          case "GET":
              if(roleInfo.read){
                 resolve(true);
              }else{
                this.router.navigate(['/sessions/404']); // Return error and route to login page
                resolve(false); // Return false: user not authorized to view page
              }
              break;
          case "POST":
              if(roleInfo.write && roleInfo.create){
                resolve(true);
              }else{
                this.router.navigate(['/sessions/404']); // Return error and route to login page
                resolve(false); // Return false: user not authorized to view page
              }
              break;
          case "PUT":
          case "PATCH":
              if(roleInfo.write && roleInfo.change){
                resolve(true);
              }else{
                this.router.navigate(['/sessions/404']); // Return error and route to login page
                resolve(false); // Return false: user not authorized to view page
              }
              break;
          case "DELETE":
              if(roleInfo.delete) {
                resolve(true);
              }else{
                this.router.navigate(['/sessions/404']); // Return error and route to login page
                resolve(false); // Return false: user not authorized to view page
              }
              break;
              default:
              resolve(true);
      }
          }, err => {
        });
      }, err => {
    });

   })
  } else {
    return true;
  }

  }
}