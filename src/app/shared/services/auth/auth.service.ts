import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Config } from '../config/general.config';
import { API_URL } from '../config/env.config';

@Injectable()
export class AuthService {
  public apiUsrRoute:string = 'users';
  apiRoleRoute:string = 'role-management';
  constructor(private _http: Http) { }


  getUserDataByID(id){
      return this._http.get(API_URL + this.apiUsrRoute + '/' + id, Config.apiAccessToken()).map(res => res.json()).catch(Config.handleError);
   }

   getRoleDataByOrgIdAndRoleName(org_id, role_name){
      return this._http.get(API_URL + this.apiRoleRoute + '/byOrgIdAndRoleName/' + org_id + '/' + role_name, Config.apiAccessToken()).map(res => res.json()).catch(Config.handleError);
   }

}