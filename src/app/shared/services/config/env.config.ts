

// if (process.env.ENV == 'production') {
 	// var URL: string = window.location.protocol + "//" + window.location.host;
 	// var SOCKET_URL: string = window.location.protocol + "//" + window.location.host;

 	var URL: string = window.location.protocol + "//" + '184.168.123.236:5050';
	var SOCKET_URL: string = window.location.protocol + "//" + 'http://184.168.123.236:5050';
// }

export const USER_ROLES = ['admin', 'supadmin'];
export const USER_GROUPS = ['private', 'public'];
export const HOST_URL: string = URL;
export const WEB_SOCKET_URL: string = SOCKET_URL;
export const API_URL: string = HOST_URL + "/api/";
export const API_LOGIN_URL: string = HOST_URL + "/auth/";
export const JSON_URL: string = HOST_URL + '/data/';
export const REQUEST_TIMEOUT=5000;

export const BUTTON_SETTING = {
	SHOW_CREATE_BUTTON: false,
	SHOW_EDIT_BUTTON: false,
	SHOW_DELETE_BUTTON: false
};
