import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";
import am4geodata_continentsLow from "@amcharts/amcharts4-geodata/continentsLow";

import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class MapChartService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query , limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
              var _self = this;
    // Create map instance
    /**
     * Define SVG path for target icon
     */

    let targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";
    // Create map instance
    let chart = am4core.create(chartDiv, am4maps.MapChart);

    let restoreContinents = function(){
      chart.goHome();
    };


    // Zoom control
    chart.zoomControl = new am4maps.ZoomControl();

    let homeButton = new am4core.Button();
    homeButton.events.on("hit", restoreContinents);

    homeButton.icon = new am4core.Sprite();
    homeButton.padding(7, 5, 7, 5);
    homeButton.width = 30;
    homeButton.icon.path = "M16,8 L14,8 L14,16 L10,16 L10,10 L6,10 L6,16 L2,16 L2,8 L0,8 L8,0 L16,8 Z M16,8";
    homeButton.marginBottom = 10;
    homeButton.parent = chart.zoomControl;
    homeButton.insertBefore(chart.zoomControl.plusButton);

    // Shared
    let hoverColorHex = "#9a7bca";
    let hoverColor = am4core.color(hoverColorHex);
    let hideCountries = function() {
      // countryTemplate.hide();
      // labelContainer.hide();
    };

    // Set map definition
    chart.geodata = am4geodata_worldLow;

    // Set projection
    chart.projection = new am4maps.projections.Miller();

    // Create map polygon series
    let polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

    // Exclude Antartica
    polygonSeries.exclude = ["AQ"];

    // Make map load polygon (like country names) data from GeoJSON
    polygonSeries.useGeodata = true;

    // Configure series
    let polygonTemplate = polygonSeries.mapPolygons.template;
    polygonTemplate.strokeOpacity = 0.5;
    polygonTemplate.nonScalingStroke = true;

    // create capital markers
    let imageSeries = chart.series.push(new am4maps.MapImageSeries());

    // define template
    let imageSeriesTemplate = imageSeries.mapImages.template;
    let circle = imageSeriesTemplate.createChild(am4core.Sprite);
    circle.scale = 1.0;
    // circle.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
    circle.fill = am4core.color("#0067a5");
    circle.path = targetSVG;
    circle.cursorOverStyle = am4core.MouseCursorStyle.pointer;

// let circle = imageSeriesTemplate.createChild(am4core.Circle);
// circle.fillOpacity = 0.7;
// circle.fill = am4core.color("#0067a5");
// circle.tooltipText = "{name}: [bold]{value}[/]";


    //click on point event
      circle.events.on("hit", function(ev) {
        var data:any; 
        // data = ev.target.dataItem.dataContext;
        // _self.city = data.city;
        // _self.Total_Amount = _self.formate(data.Total_Amount);
        // _self.count_no = data.count_no;
        // _self.counter_code = data.id;
        // _self.cdr.detectChanges();
    }, this);
    // what about scale...

    // set propertyfields
    imageSeriesTemplate.propertyFields.latitude = visualizationOptionData.latitude;
    imageSeriesTemplate.propertyFields.longitude = visualizationOptionData.longitude;

    imageSeriesTemplate.horizontalCenter = "middle";
    imageSeriesTemplate.verticalCenter = "middle";
    imageSeriesTemplate.align = "center";
    // imageSeriesTemplate.zoomControl ={buttonFillColor:"#15A892"},
    imageSeriesTemplate.valign = "middle";
    imageSeriesTemplate.width = 40;
    imageSeriesTemplate.height = 40;
    imageSeriesTemplate.nonScaling = true;
    imageSeriesTemplate.tooltipText = "{"+ visualizationOptionData.seriestooltipText + "}";
    imageSeriesTemplate.fill = am4core.color("#0067a5");

    imageSeriesTemplate.background.fillOpacity = 0;
    imageSeriesTemplate.background.fill = am4core.color("#ffffff");
    imageSeriesTemplate.setStateOnChildren = true;
    imageSeriesTemplate.states.create("hover");

    imageSeries.data = chartData;
        return chart;
    }
}