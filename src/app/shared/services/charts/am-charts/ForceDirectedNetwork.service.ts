import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4plugins_forceDirected from "@amcharts/amcharts4/plugins/forceDirected";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class ForceDirectedNetworkChartService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

    //------------------------------------------------Methods------------------------------------------//

    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
              this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        let chart = am4core.create(chartDiv, am4plugins_forceDirected.ForceDirectedTree);
        let networkSeries = chart.series.push(new am4plugins_forceDirected.ForceDirectedSeries())
        networkSeries.dataFields.linkWith = visualizationOptionData.selectedLinkWith;
        networkSeries.dataFields.name = visualizationOptionData.selectedCategory;
        networkSeries.dataFields.id = visualizationOptionData.selectedCategory;
        networkSeries.dataFields.value = visualizationOptionData.selectedValue;
        networkSeries.dataFields.children = visualizationOptionData.selectedChildren;

        networkSeries.nodes.template.label.text = "{"+visualizationOptionData.selectedCategory+"}"
        networkSeries.fontSize = 8;
        networkSeries.linkWithStrength = 0;

        let nodeTemplate = networkSeries.nodes.template;
        nodeTemplate.tooltipText = "{"+visualizationOptionData.selectedCategory+"}";
        nodeTemplate.fillOpacity = 1;
        nodeTemplate.label.hideOversized = true;
        nodeTemplate.label.truncate = true;

        let linkTemplate = networkSeries.links.template;
        linkTemplate.strokeWidth = 1;
        let linkHoverState = linkTemplate.states.create("hover");
        linkHoverState.properties.strokeOpacity = 1;
        linkHoverState.properties.strokeWidth = 2;

        nodeTemplate.events.on("over", function(event) {
            let dataItem = event.target.dataItem;
            dataItem.childLinks.each(function(link) {
                link.isHover = true;
            })
        })

        nodeTemplate.events.on("out", function(event) {
            let dataItem = event.target.dataItem;
            dataItem.childLinks.each(function(link) {
                link.isHover = false;
            })
        })

        nodeTemplate.events.on("hit", function (event) {
            let dataItem = event.target.dataItem;
            dataItem.childLinks.each(function (link) {
                link.isHover = true;
            })


            // var dataContext:any
            //       dataContext = ev.target.dataItem.category;
            //       let query_obj = visualizationData.visualization_leaf.query_obj;
            //       if (visualizationData.visualization_leaf.query_obj != undefined) {
            //           query_obj[visualizationOptionData.selectedCategory] = dataContext;
            //       }
              // _self._router.navigate(['dashboard/leaf/' 
              //                + visualizationData.visualization_id + '/' + _self.createStringUrl(query_obj)]);
        })




        networkSeries.data = chartData;
        return chart

    }

    createStringUrl(obj) {
        var stringUrl = ""; 
          for (var key in obj) { 
              if (stringUrl != "") { 
                  stringUrl += "&"; 
              } 
              stringUrl += (key + "=" + encodeURIComponent(obj[key])); 
          } 
    return stringUrl
  }
}