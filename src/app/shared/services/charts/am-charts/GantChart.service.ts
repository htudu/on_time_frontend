import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class GantChartService  {

    constructor(private _objectService: ChartDataService) {}



//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, data_source_id, collectionDataName, query , limit) {
        // this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination('schedule_Data', query)
        //     .subscribe(res => {


         
        //       this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
        //       console.log('in the gantchart service make chart we are printing the result data list')
        //       console.log(res)
        //     }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {

         


        let chart = am4core.create(chartDiv, am4charts.XYChart);
chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

chart.paddingRight = 30;
chart.dateFormatter.inputDateFormat = "yyyy-MM-dd HH:mm";

let colorSet = new am4core.ColorSet();
colorSet.saturation = 0.4;

        console.log(chartData)
"evening"
       let chartCustData=[]
        for(let i=0;i<chartData.length;i++){
          // let date= new Date(chartData[i].plan_start_date) ;
           //console.log(date.getFullYear()+'-'+date.getMonth()+'-'+date.getDay())
             

             if (chartData[i].shift == "morning") {
                    chartCustData.push({
                    category:chartData[i].name,
                    start:chartData[i].fromDate,
                    end: chartData[i].toDate,
                    color: colorSet.getIndex(6).brighten(1.4),
                    shift: chartData[i].shift,
                    plan: chartData[i].plan
                })
             } else if (chartData[i].shift == "night") {
                 chartCustData.push({
                    category:chartData[i].name,
                    start:chartData[i].fromDate,
                    end: chartData[i].toDate,
                    color: colorSet.getIndex(0).brighten(1),
                    shift: chartData[i].shift,
                    plan: chartData[i].plan

                })

             } else {
                 chartCustData.push({
                    category:chartData[i].name,
                    start:chartData[i].fromDate,
                    end: chartData[i].toDate,
                    color: colorSet.getIndex(8).brighten(1),
                    shift: chartData[i].shift,
                    plan: chartData[i].plan

                })
             }
        }
        console.log(chartCustData)
chart.data = chartCustData;

// chart.data = [ {
//   "category": "Module #1",
//   "start": "2016-01-01",
//   "end": "2016-01-02",
  // "color": colorSet.getIndex(0).brighten(0),
//   "task": "Gathering requirements"
// }, {
//   "category": "Module #1",
//   "start": "2016-01-3",
//   "end": "2016-01-04",
//   "color": colorSet.getIndex(0).brighten(0.4),
//   "task": "Producing specifications"
// }, {
//   "category": "Module #1",
//   "start": "2016-02-05",
//   "end": "2016-04-18",
//   "color": colorSet.getIndex(0).brighten(0.8),
//   "task": "Development"
// }, {
//   "category": "Module #1",
//   "start": "2016-04-18",
//   "end": "2016-04-30",
//   "color": colorSet.getIndex(0).brighten(1.2),
//   "task": "Testing and QA"
// }, {
//   "category": "Module #2",
//   "start": "2016-01-08",
//   "end": "2016-01-10",
//   "color": colorSet.getIndex(2).brighten(0),
//   "task": "Gathering requirements"
// }, {
//   "category": "Module #2",
//   "start": "2016-01-12",
//   "end": "2016-01-15",
//   "color": colorSet.getIndex(2).brighten(0.4),
//   "task": "Producing specifications"
// }, {
//   "category": "Module #2",
//   "start": "2016-01-16",
//   "end": "2016-02-05",
//   "color": colorSet.getIndex(2).brighten(0.8),
//   "task": "Development"
// }, {
//   "category": "Module #2",
//   "start": "2016-02-10",
//   "end": "2016-02-18",
//   "color": colorSet.getIndex(2).brighten(1.2),
//   "task": "Testing and QA"
// }, {
//   "category": "Module #3",
//   "start": "2016-01-02",
//   "end": "2016-01-08",
//   "color": colorSet.getIndex(4).brighten(0),
//   "task": "Gathering requirements"
// }, {
//   "category": "Module #3",
//   "start": "2016-01-08",
//   "end": "2016-01-16",
//   "color": colorSet.getIndex(4).brighten(0.4),
//   "task": "Producing specifications"
// }, {
//   "category": "Module #3",
//   "start": "2016-01-19",
//   "end": "2016-03-01",
//   "color": colorSet.getIndex(4).brighten(0.8),
//   "task": "Development"
// }, {
//   "category": "Module #3",
//   "start": "2016-03-12",
//   "end": "2016-04-05",
//   "color": colorSet.getIndex(4).brighten(1.2),
//   "task": "Testing and QA"
// }, {
//   "category": "Module #4",
//   "start": "2016-01-01",
//   "end": "2016-01-19",
//   "color": colorSet.getIndex(6).brighten(0),
//   "task": "Gathering requirements"
// }, {
//   "category": "Module #4",
//   "start": "2016-01-19",
//   "end": "2016-02-03",
//   "color": colorSet.getIndex(6).brighten(0.4),
//   "task": "Producing specifications"
// }, {
//   "category": "Module #4",
//   "start": "2016-03-20",
//   "end": "2016-04-25",
//   "color": colorSet.getIndex(6).brighten(0.8),
//   "task": "Development"
// }, {
//   "category": "Module #4",
//   "start": "2016-04-27",
//   "end": "2016-05-15",
//   "color": colorSet.getIndex(6).brighten(1.2),
//   "task": "Testing and QA"
// }, {
//   "category": "Module #5",
//   "start": "2016-01-01",
//   "end": "2016-01-12",
//   "color": colorSet.getIndex(8).brighten(0),
//   "task": "Gathering requirements"
// }, {
//   "category": "Module #5",
//   "start": "2016-01-12",
//   "end": "2016-01-19",
//   "color": colorSet.getIndex(8).brighten(0.4),
//   "task": "Producing specifications"
// }, {
//   "category": "Module #5",
//   "start": "2016-01-19",
//   "end": "2016-03-01",
//   "color": colorSet.getIndex(8).brighten(0.8),
//   "task": "Development"
// }, {
//   "category": "Module #5",
//   "start": "2016-03-08",
//   "end": "2016-03-30",
//   "color": colorSet.getIndex(8).brighten(1.2),
//   "task": "Testing and QA"
// } ];

chart.dateFormatter.dateFormat = "yyyy-MM-dd";
chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";

let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
categoryAxis.dataFields.category = "category";
// categoryAxis.renderer.grid.template.location = 0;
categoryAxis.renderer.inversed = true;

let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
dateAxis.renderer.minGridDistance = 70;
// dateAxis.baseInterval = { count: 1, timeUnit: "day" };
// dateAxis.max = new Date(2018, 0, 1, 24, 0, 0, 0).getTime();
//dateAxis.strictMinMax = true;
dateAxis.renderer.tooltipLocation = 0;

let series1 = chart.series.push(new am4charts.ColumnSeries());
series1.columns.template.height = am4core.percent(70);
// series1.columns.template.tooltipText = "Re{bold]{openDateX}[/] - [bold]{dateX}[/]";
series1.columns.template.tooltipText = "{category}/{shift}, {plan}: {openDateX}";

series1.dataFields.openDateX = "start";
series1.dataFields.dateX = "end";
series1.dataFields.categoryY = "category";
series1.columns.template.propertyFields.fill = "color"; // get color from data
series1.columns.template.propertyFields.stroke = "color";
series1.columns.template.strokeOpacity = 1;

chart.scrollbarX = new am4core.Scrollbar();

        return chart;

        // // Create chart instance
        // //let chart= am4core.create(chartDiv,am4charts.TreeMap)
        // let chart = am4core.create(chartDiv, am4charts.XYChart);
        // chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        
        // chart.paddingRight = 30;
        // chart.dateFormatter.inputDateFormat = "yyyy-MM-dd HH:mm:ss";
        
        // let colorSet = new am4core.ColorSet();
        // colorSet.saturation = 0.4;
        // //  let chartCustData=[]
        // // for(let i=0;i<chartData.length;i++){
        // //   // let date= new Date(chartData[i].plan_start_date) ;
        // //    //console.log(date.getFullYear()+'-'+date.getMonth()+'-'+date.getDay())
        // //      chartCustData.push({
        // //         name:chartData[i].name,
        // //         fromDate:chartData[i].fromDate,
        // //         toDate: chartData[i].toDate,
        // //         color: colorSet.getIndex(0).brighten(0)
        // //      })
        // // }
        // // console.log(chartCustData)

        // // chart.data = [
        // //   {
        // //     name: "John",
        // //     fromDate: "2018-01-01 08:00:00",
        // //     toDate: "2018-01-01 10:00:00",
        // //     color: colorSet.getIndex(0).brighten(0)
        // //   },
        // //   {
        // //     name: "Johny",
        // //     fromDate: "2018-01-01 12:00:00",
        // //     toDate: "2018-01-01 15:00:00",
        // //     color: colorSet.getIndex(0).brighten(0.4)
        // //   },
          
        // // ];
        
        // chart.data = [
        //   {
        //     name: "John",
        //     fromDate: "2018-01-01 08:00:00",
        //     toDate: "2018-01-01 10:00:00",
        //     color: colorSet.getIndex(0).brighten(0)
        //   },
        //   {
        //     name: "John",
        //     fromDate: "2018-01-01 12:00:00",
        //     toDate: "2018-01-01 15:00:00",
        //     color: colorSet.getIndex(0).brighten(0.4)
        //   },
        //   {
        //     name: "John",
        //     fromDate: "2018-01-01 15:30:00",
        //     toDate: "2018-01-01 21:30:00",
        //     color: colorSet.getIndex(0).brighten(0.8)
        //   },
        
        //   {
        //     name: "Jane",
        //     fromDate: "2018-01-01 09:00:00",
        //     toDate: "2018-01-01 12:00:00",
        //     color: colorSet.getIndex(2).brighten(0)
        //   },
        //   {
        //     name: "Jane",
        //     fromDate: "2018-01-01 13:00:00",
        //     toDate: "2018-01-01 17:00:00",
        //     color: colorSet.getIndex(2).brighten(0.4)
        //   },
        
        //   {
        //     name: "Peter",
        //     fromDate: "2018-01-01 11:00:00",
        //     toDate: "2018-01-01 16:00:00",
        //     color: colorSet.getIndex(4).brighten(0)
        //   },
        //   {
        //     name: "Peter",
        //     fromDate: "2018-01-01 16:00:00",
        //     toDate: "2018-01-01 20:00:00",
        //     color: colorSet.getIndex(4).brighten(0.4)
        //   },
        
        //   {
        //     name: "Melania",
        //     fromDate: "2018-01-01 16:00:00",
        //     toDate: "2018-01-01 20:00:00",
        //     color: colorSet.getIndex(6).brighten(0)
        //   },
        //   {
        //     name: "Melania",
        //     fromDate: "2018-01-01 20:30:00",
        //     toDate: "2018-01-01 24:00:00",
        //     color: colorSet.getIndex(6).brighten(0.4)
        //   },
        
        //   {
        //     name: "Donald",
        //     fromDate: "2018-01-01 13:00:00",
        //     toDate: "2018-01-01 24:00:00",
        //     color: colorSet.getIndex(8).brighten(0)
        //   }
        // ];
        // // chart.data=chartCustData
        
        // let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis());
        // categoryAxis.dataFields.category = "name";
        // categoryAxis.renderer.grid.template.location = 0;
        // categoryAxis.renderer.inversed = true;
        
        // let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        // dateAxis.dateFormatter.dateFormat = "yyyy-MM-dd";
        // dateAxis.renderer.minGridDistance = 70;
        // dateAxis.baseInterval = { count: 30, timeUnit: "hour" };
        // dateAxis.max = new Date(2019, 4, 1, 24, 0, 0, 0).getTime();
        // dateAxis.strictMinMax = true;
        // dateAxis.renderer.tooltipLocation = 0;
        
        // let series1 = chart.series.push(new am4charts.ColumnSeries());
        // series1.columns.template.width = am4core.percent(80);
        // series1.columns.template.tooltipText = "{name}: {openDateX} - {dateX}";
        
        // series1.dataFields.openDateX = "fromDate";
        // series1.dataFields.dateX = "toDate";
        // series1.dataFields.categoryY = "name";
        // series1.columns.template.propertyFields.fill = "color"; // get color from data
        // series1.columns.template.propertyFields.stroke = "color";
        // series1.columns.template.strokeOpacity = 1;
        
        // chart.scrollbarX = new am4core.Scrollbar();
        
        
    }
}