import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class LineChartService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

    //------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
                this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
            }, err => {});
    }

    generateChartData() {
        let chartData = [];
        let firstDate = new Date();
        firstDate.setDate(firstDate.getDate() - 100);
        firstDate.setHours(0, 0, 0, 0);

        let visits = 1600;
        let hits = 2900;
        let views = 8700;

        for (var i = 0; i < 15; i++) {
            // we create date objects here. In your data, you can have date strings
            // and then set format of your dates using chart.dataDateFormat property,
            // however when possible, use date objects, as this will speed up chart rendering.
            let newDate = new Date(firstDate);
            newDate.setDate(newDate.getDate() + i);

            visits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
            hits += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);
            views += Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

            chartData.push({
                date: newDate,
                visits: visits,
                hits: hits,
                views: views
            });
        }
        return chartData;
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        var _self = this;
        var seriesField = visualizationOptionData.seriesField.seriesField

        // Create chart instance
        var chart = am4core.create(chartDiv, am4charts.XYChart);

        // Increase contrast by taking evey second color
        chart.colors.step = 2;



        // Create axes
        let dateAxis = chart.xAxes.push(new am4charts.DateAxis());
        dateAxis.renderer.minGridDistance = 50;
        // Add data
        var data = this.generateChartData();
        // Create series
        function createAxisAndSeries(field, name, opposite, bullet) {
            let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

            let series = chart.series.push(new am4charts.LineSeries());
            series.dataFields.valueY = field;
            // series.dataFields.dateX = "date";
            series.dataFields.dateX = visualizationOptionData.selectedCategory;

            series.strokeWidth = 2;
            series.yAxis = valueAxis;
            series.name = name;
            series.tooltipText = "{name}: [bold]{valueY}[/]";
            series.tensionX = 0.8;

            let interfaceColors = new am4core.InterfaceColorSet();

            switch (bullet) {
                case "triangle":
                    let bullet_t = series.bullets.push(new am4charts.Bullet());
                    bullet_t.width = 12;
                    bullet_t.height = 12;
                    bullet_t.horizontalCenter = "middle";
                    bullet_t.verticalCenter = "middle";

                    let triangle = bullet_t.createChild(am4core.Triangle);
                    triangle.stroke = interfaceColors.getFor("background");
                    triangle.strokeWidth = 2;
                    triangle.direction = "top";
                    triangle.width = 12;
                    triangle.height = 12;
                    break;
                case "rectangle":
                    let bullet_r = series.bullets.push(new am4charts.Bullet());
                    bullet_r.width = 10;
                    bullet_r.height = 10;
                    bullet_r.horizontalCenter = "middle";
                    bullet_r.verticalCenter = "middle";

                    let rectangle = bullet_r.createChild(am4core.Rectangle);
                    rectangle.stroke = interfaceColors.getFor("background");
                    rectangle.strokeWidth = 2;
                    rectangle.width = 10;
                    rectangle.height = 10;
                    break;
                default:
                    let bullet_d = series.bullets.push(new am4charts.CircleBullet());
                    bullet_d.circle.stroke = interfaceColors.getFor("background");
                    bullet_d.circle.strokeWidth = 2;
                    break;
            }

            valueAxis.renderer.line.strokeOpacity = 1;
            valueAxis.renderer.line.strokeWidth = 2;
            valueAxis.renderer.line.stroke = series.stroke;
            valueAxis.renderer.labels.template.fill = series.stroke;
            valueAxis.renderer.opposite = opposite;
            valueAxis.renderer.grid.template.disabled = true;
        }

        for (var i = 0; i < seriesField.length; i++) {
            createAxisAndSeries(seriesField[i].field, seriesField[i].name, seriesField[i].opposite, seriesField[i].bullet);
        }

        chart.data = chartData;

        // Add legend
        // Add a legend
        if (visualizationOptionData.legend) {
            chart.legend = new am4charts.Legend();
        }

        // Add cursor
        chart.cursor = new am4charts.XYCursor();

        return chart;
    }
}