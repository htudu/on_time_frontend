import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ToastrService
} from 'ngx-toastr';

import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class SolidGaugeService implements OnInit {

    constructor(private _objectService: ChartDataService, private _router: Router, private toastrService: ToastrService) {}

    ngOnInit() {}
      am4themes_cs(target) {
  // if (target instanceof am4core.ColorSet) {
  //   target.list = [
  //     am4core.color("#33b7af"),
  //     am4core.color("#6f87d8"),
  //     am4core.color("#bc52bc"),
  //     am4core.color("#663db8"),
  //     am4core.color("#57c17b"),
  //     am4core.color("rgb(190, 180, 230)"),
  //     am4core.color("#FFA500"),
  //     am4core.color("#FF7D00"),
  //     am4core.color("#EE5A30"),
  //     am4core.color("#D046B6"),                                                                     
  //     am4core.color("#653789"),
  //     am4core.color("#2A49A0")
  //   ];
  // }
  if (target instanceof am4core.Tooltip) {
    target.getFillFromObject = false;
    target.getStrokeFromObject = false;
    target.background.strokeOpacity = 0.4;
    target.background.fill = am4core.color("#000");
  }
  if (target instanceof am4core.Label) {
    target.fill = am4core.color("#555");
  }
  // if (target instanceof am4charts.Axis) {
  //   target.cursorTooltipEnabled = false;
  // }
  if (target instanceof am4charts.AxisRendererY) {
    target.grid.template.strokeOpacity = 0.1;
    // target.line.strokeOpacity = 1;
  }
  if (target instanceof am4charts.Grid) {
    target.strokeOpacity = 0.1;
    target.stroke = am4core.color("#000");
  }
  if (target instanceof am4core.InterfaceColorSet) {
    target.setFor("text", am4core.color("#333"));
    target.setFor("primaryButton", am4core.color("#666"));
    target.setFor("primaryButtonHover", am4core.color("#1FBCFF"));
    target.setFor("primaryButtonDown", am4core.color("#1FBCFF").lighten(-0.2));
    target.setFor(
      "primaryButtonActive",
      am4core.color("#1FBCFF").lighten(-0.2)
    );
    // target.setFor("primaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("primaryButtonStroke", am4core.color("#467B88"));
    // target.setFor("secondaryButton", am4core.color("#6DC0D5"));
    // target.setFor("secondaryButtonHover", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonDown", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonActive", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("secondaryButtonStroke", am4core.color("#467B88"));
    // stroke
    // fill
    // disabledBackground
    // positive
    // negative
    // alternativeText
    // background
    // alternativeBackground
    // grid
    //target.setFor("grid", am4core.color("#ebebeb"));
  }
}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
        .subscribe(res => {
          if (res.dataList.length != 0) {
            let chartData = res.dataList;

                for (var i = chartData.length - 1; i >= 0; i--) {
                  chartData[i]["full"] = 100;
                }
                this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData)
                } else {
                    this.toastrService.error('NO Data Found!'); // Used to notify error responce
                }
          }, err => {
                this.toastrService.error(err); // Used to notify error responce
        });
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
        am4core.useTheme(this.am4themes_cs);
        var _self = this;
        // Create chart instance
        let chart = am4core.create(chartDiv, am4charts.RadarChart);

        // Add data
        chart.data = chartData;

        // Make chart not full circle
        chart.startAngle = -90;
        chart.endAngle = 180;
        chart.innerRadius = am4core.percent(20);

        // Set number format
        chart.numberFormatter.numberFormat = "#.#'%'";

        // Create axes
        let categoryAxis = chart.yAxes.push(new am4charts.CategoryAxis() as any);
        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;
        categoryAxis.renderer.grid.template.location = 0;
        categoryAxis.renderer.grid.template.strokeOpacity = 0;
        categoryAxis.renderer.labels.template.horizontalCenter = "right";
        categoryAxis.renderer.labels.template.fontWeight = 500;
        categoryAxis.renderer.labels.template.adapter.add("fill", function(fill, target) {
          return (target.dataItem.index >= 0) ? chart.colors.getIndex(target.dataItem.index) : fill;
        });
        categoryAxis.renderer.minGridDistance = 10;

        let valueAxis = chart.xAxes.push(new am4charts.ValueAxis() as any);
        valueAxis.renderer.grid.template.strokeOpacity = 0;
        valueAxis.min = 0;
        valueAxis.max = 100;
        valueAxis.strictMinMax = true;

        // Create series
        let series1 = chart.series.push(new am4charts.RadarColumnSeries() as any);
        series1.dataFields.valueX = "full";
        series1.dataFields.categoryY = visualizationOptionData.selectedCategory;
        series1.clustered = false;
        series1.columns.template.fill = new am4core.InterfaceColorSet().getFor("alternativeBackground");
        series1.columns.template.fillOpacity = 0.08;
        series1.columns.template.cornerRadiusTopLeft = 20;
        series1.columns.template.strokeWidth = 0;
        series1.columns.template.radarColumn.cornerRadius = 20;

        let series2 = chart.series.push(new am4charts.RadarColumnSeries());
        series2.dataFields.valueX = visualizationOptionData.selectedValue;
        series2.dataFields.categoryY = visualizationOptionData.selectedCategory;
        series2.clustered = false;
        series2.columns.template.strokeWidth = 0;
        series2.columns.template.tooltipText = "{"+visualizationOptionData.selectedCategory+"}: [bold]{"+visualizationOptionData.selectedValue+"}[/]";
        series2.columns.template.radarColumn.cornerRadius = 20;
        
        if (visualizationData != undefined) {
          if (visualizationData.visualization_leaf.visualization_id != undefined) {
              series2.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
              series2.columns.template.events.on("hit", function(ev) {
              let dataContext:any
              dataContext = ev.target.dataItem.dataContext;
              let dataFields:any
              dataFields = ev.target.dataItem.component.dataFields;
              let query_obj = visualizationData.visualization_leaf.query_obj;
              query_obj[dataFields.categoryY] = dataContext[dataFields.categoryY];
               _self._router.navigate(['dashboard/leaf/' 
                             + visualizationData.visualization_id + '/' + _self.createStringUrl(query_obj)]);

            }, this);
          }
        }  

        series2.columns.template.adapter.add("fill", function(fill, target) {
          return chart.colors.getIndex(target.dataItem.index);
        });

        // Add cursor
        chart.cursor = new am4charts.RadarCursor();

        return chart;
    }
  createStringUrl(obj) {
    var stringUrl = ""; 
      for (var key in obj) { 
          if (stringUrl != "") { 
              stringUrl += "&"; 
          } 
          stringUrl += (key + "=" + encodeURIComponent(obj[key])); 
      } 
    return stringUrl
  }
}

