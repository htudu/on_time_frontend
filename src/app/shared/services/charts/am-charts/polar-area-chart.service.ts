import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class PolarAreaChartService implements OnInit {

    constructor(private _objectService: ChartDataService) {}

    ngOnInit() {}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationOptionData, chartDiv, data_source_id, collectionDataName, query , limit) {
        // this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(data_source_id, collectionDataName, query)
        //     .subscribe(res => {

        //       this.createChartInstance(visualizationOptionData, chartDiv, res.dataList)
        //     }, err => {});
    }

    createChartInstance(visualizationOptionData, chartDiv, chartData) {
        var _self = this;

        let chart = am4core.create("chartdiv", am4charts.RadarChart);
        chart.hiddenState.properties.opacity = 0; // this creates initial fade-in
        chart.legend = new am4charts.Legend();

        chart.colors.list = [
            am4core.color("#f3c300"),
            am4core.color("#875692")
        ];

        chart.padding(20, 20, 20, 20);
        chart.data = chartData;
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis() as any);
        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;
        categoryAxis.renderer.labels.template.location = 0.5;
        categoryAxis.renderer.tooltipLocation = 0.5;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);
        valueAxis.tooltip.disabled = true;
        valueAxis.renderer.labels.template.horizontalCenter = "left";
        valueAxis.min = 0;

        let series1 = chart.series.push(new am4charts.RadarColumnSeries());
        series1.columns.template.tooltipText = "{name}: {valueY.value}";
        series1.columns.template.width = am4core.percent(80);
        series1.name = visualizationOptionData.selectedseriesValueY;
        series1.dataFields.categoryX = visualizationOptionData.selectedCategory;
        series1.dataFields.valueY = visualizationOptionData.selectedseriesValueY;
        series1.stacked = true;
        series1.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        // series1.columns.template.events.on("hit", function(ev) {
        //    _self._router.navigate(['/n-2-analytics/script-table/zbsitif_5_tr_analysis_b-' + _self.n1id.split("-")[5]]);
        // }, this);

        // let series2 = chart.series.push(new am4charts.RadarColumnSeries());
        // series2.columns.template.width = am4core.percent(80);
        // series2.columns.template.tooltipText = "{name}: {valueY.value}";
        // series2.name = "INDIA";
        // series2.dataFields.categoryX =  visualizationOptionData.selectedCategory;
        // series2.dataFields.valueY = "hana";
        // series2.stacked = true;
        // series2.columns.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
        // // series2.columns.template.events.on("hit", function(ev) {
        // //    _self._router.navigate(['/n-2-analytics/script-table/zbsitif_5_tr_analysis_b-' + _self.n1id.split("-")[5]]);
        // // }, this);




        chart.seriesContainer.zIndex = -1;

        chart.scrollbarX = new am4core.Scrollbar();
        chart.scrollbarX.exportable = false;
        chart.scrollbarY = new am4core.Scrollbar();
        chart.scrollbarY.exportable = false;

        chart.cursor = new am4charts.RadarCursor();
        chart.cursor.xAxis = categoryAxis;
        chart.cursor.fullWidthLineX = true;
        chart.cursor.lineX.strokeOpacity = 0;
        chart.cursor.lineX.fillOpacity = 0.1;
        chart.cursor.lineX.fill = am4core.color("#000000");

        // /* Create chart instance */
        // var chart = am4core.create(chartDiv, am4charts.RadarChart);

        // /* Add data */
        // chart.data = chartData;

        // /* Create axes */
        // let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis() as any);
        // categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;

        // let valueAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);
        // valueAxis.extraMin = 0.2;
        // valueAxis.extraMax = 0.2;
        // valueAxis.tooltip.disabled = true;

        // /* Create and configure series */
        // let series = chart.series.push(new am4charts.RadarSeries());
        // series.dataFields.valueY = visualizationOptionData.selectedseriesValueY;
        // series.dataFields.categoryX = visualizationOptionData.selectedCategory;
        // series.strokeWidth = 3;
        // series.tooltipText = "{valueY}";
        // series.name = visualizationOptionData.selectedseriesValueY;
        // series.bullets.create(am4charts.CircleBullet);
        // chart.cursor = new am4charts.RadarCursor();

        return chart;
    }
}