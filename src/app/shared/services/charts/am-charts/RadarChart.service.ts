import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';

import {
    ToastrService
} from 'ngx-toastr';

import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import am4themes_animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(am4themes_animated);

@Injectable()
export class RadarChartService implements OnInit {

    constructor(private _objectService: ChartDataService, private _router: Router, private toastrService: ToastrService) {}

    ngOnInit() {}
      am4themes_cs(target) {
  // if (target instanceof am4core.ColorSet) {
  //   target.list = [
  //     am4core.color("#33b7af"),
  //     am4core.color("#6f87d8"),
  //     am4core.color("#bc52bc"),
  //     am4core.color("#663db8"),
  //     am4core.color("#57c17b"),
  //     am4core.color("rgb(190, 180, 230)"),
  //     am4core.color("#FFA500"),
  //     am4core.color("#FF7D00"),
  //     am4core.color("#EE5A30"),
  //     am4core.color("#D046B6"),                                                                     
  //     am4core.color("#653789"),
  //     am4core.color("#2A49A0")
  //   ];
  // }
  if (target instanceof am4core.Tooltip) {
    target.getFillFromObject = false;
    target.getStrokeFromObject = false;
    target.background.strokeOpacity = 0.4;
    target.background.fill = am4core.color("#000");
  }
  if (target instanceof am4core.Label) {
    target.fill = am4core.color("#555");
  }
  // if (target instanceof am4charts.Axis) {
  //   target.cursorTooltipEnabled = false;
  // }
  if (target instanceof am4charts.AxisRendererY) {
    target.grid.template.strokeOpacity = 0.1;
    // target.line.strokeOpacity = 1;
  }
  if (target instanceof am4charts.Grid) {
    target.strokeOpacity = 0.1;
    target.stroke = am4core.color("#000");
  }
  if (target instanceof am4core.InterfaceColorSet) {
    target.setFor("text", am4core.color("#333"));
    target.setFor("primaryButton", am4core.color("#666"));
    target.setFor("primaryButtonHover", am4core.color("#1FBCFF"));
    target.setFor("primaryButtonDown", am4core.color("#1FBCFF").lighten(-0.2));
    target.setFor(
      "primaryButtonActive",
      am4core.color("#1FBCFF").lighten(-0.2)
    );
    // target.setFor("primaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("primaryButtonStroke", am4core.color("#467B88"));
    // target.setFor("secondaryButton", am4core.color("#6DC0D5"));
    // target.setFor("secondaryButtonHover", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonDown", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonActive", am4core.color("#6DC0D5").lighten(-0.2));
    // target.setFor("secondaryButtonText", am4core.color("#FFFFFF"));
    // target.setFor("secondaryButtonStroke", am4core.color("#467B88"));
    // stroke
    // fill
    // disabledBackground
    // positive
    // negative
    // alternativeText
    // background
    // alternativeBackground
    // grid
    //target.setFor("grid", am4core.color("#ebebeb"));
  }
}

//------------------------------------------------Methods------------------------------------------//


    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
        .subscribe(res => {
          if (res.dataList.length != 0) {
                    this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, res.dataList)
                } else {
                    this.toastrService.error('NO Data Found!'); // Used to notify error responce
                }
          }, err => {
                this.toastrService.error(err); // Used to notify error responce
        });
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
        am4core.useTheme(this.am4themes_cs);
        var _self = this;
        /* Create chart instance */
        var chart = am4core.create(chartDiv, am4charts.RadarChart);

        /* Add data */
        chart.data = chartData;

        /* Create axes */
        let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis() as any);
        categoryAxis.dataFields.category = visualizationOptionData.selectedCategory;

        let valueAxis = chart.yAxes.push(new am4charts.ValueAxis() as any);
        valueAxis.extraMin = 0.2;
        valueAxis.extraMax = 0.2;
        valueAxis.tooltip.disabled = true;

        /* Create and configure series */
        let series = chart.series.push(new am4charts.RadarSeries());
        series.dataFields.valueY = visualizationOptionData.selectedseriesValueY;
        series.dataFields.categoryX = visualizationOptionData.selectedCategory;
        series.strokeWidth = 3;
        series.tooltipText = "{valueY}";
        series.name = visualizationOptionData.selectedseriesValueY;
        if (visualizationData.visualization_leaf.visualization_id != undefined) {
          let bullet = series.bullets.create(am4charts.CircleBullet)
          bullet.cursorOverStyle = am4core.MouseCursorStyle.pointer;
          bullet.events.on("hit", function(ev) {
            // _self._router.navigate(['dashboard/leaf/' 
            //            + visualizationData.visualization_id], {state: {data: {user: 'vikash'}}});

          _self._router.navigateByUrl('dashboard/leaf/' 
                       + visualizationData.visualization_id, { state: { hello: 'world' } });
          });
          
        } else {
          series.bullets.create(am4charts.CircleBullet)
        }

        chart.cursor = new am4charts.RadarCursor();

        return chart;
    }
}