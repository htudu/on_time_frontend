import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectorRef,
    Injectable
} from '@angular/core';
import {
    ToastrService
} from 'ngx-toastr';

import {
    Router,
    ActivatedRoute,
    Params
} from '@angular/router';

import {
    ChartDataService
} from '../chart-data.service';

import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import animated from "@amcharts/amcharts4/themes/animated";
am4core.useTheme(animated);

import dataviz from "@amcharts/amcharts4/themes/dataviz";
import material from "@amcharts/amcharts4/themes/material";
import frozen from "@amcharts/amcharts4/themes/frozen";
import kelly from "@amcharts/amcharts4/themes/kelly";
import moonrisekingdom from "@amcharts/amcharts4/themes/moonrisekingdom";
import spiritedaway from "@amcharts/amcharts4/themes/spiritedaway";
import amcharts from "@amcharts/amcharts4/themes/amcharts";



@Injectable()
export class PieChartService implements OnInit {
    constructor(private _objectService: ChartDataService, private _router: Router,  private toastrService: ToastrService) {}
    ngOnInit() {}

am4themes_cs(target) {
  if (target instanceof am4core.ColorSet) {
    target.list = [
      am4core.color("#86ce86"),
      am4core.color("#0975da"),
      am4core.color("#0996f2"),
      am4core.color("#1fb0ff"),
      am4core.color("#41baff"),
      am4core.color("#5ec5ff"),
      am4core.color("#3db7ff"),
      am4core.color("#FF7D00"),
      am4core.color("#EE5A30"),
      am4core.color("#D046B6"),                                                                     
      am4core.color("#653789"),
      am4core.color("#2A49A0")
    ];
  }

  if (target instanceof am4core.Tooltip) {
    target.getFillFromObject = false;
    target.getStrokeFromObject = false;
    target.background.strokeOpacity = 0.4;
    target.background.fill = am4core.color("#000");
  }
  if (target instanceof am4core.Label) {
    target.fill = am4core.color("#555");
  }
  // if (target instanceof am4charts.Axis) {
  //   target.cursorTooltipEnabled = false;
  // }
  if (target instanceof am4charts.AxisRendererY) {
    target.grid.template.strokeOpacity = 0.1;
    // target.line.strokeOpacity = 1;
  }
  if (target instanceof am4charts.Grid) {
    target.strokeOpacity = 0.1;
    target.stroke = am4core.color("#000");
  }
  if (target instanceof am4core.InterfaceColorSet) {
    target.setFor("text", am4core.color("#333"));
    target.setFor("primaryButton", am4core.color("#666"));
    target.setFor("primaryButtonHover", am4core.color("#1FBCFF"));
    target.setFor("primaryButtonDown", am4core.color("#1FBCFF").lighten(-0.2));
    target.setFor(
      "primaryButtonActive",
      am4core.color("#1FBCFF").lighten(-0.2)
    );
  }
}
//------------------------------------------------Methods------------------------------------------//

    makeChart(visualizationData, visualizationOptionData, chartDiv, datasource_id, collectionDataName, query, limit) {
        let queryObj = {}
        if (query != undefined) {
            queryObj = query
        }
        this._objectService.getDataByCollectionNameWithDocumentFieldsNoPagination(datasource_id, collectionDataName, queryObj, limit)
            .subscribe(res => {
                if (res.dataList.length != 0) {
                    this.createChartInstance(visualizationData, visualizationOptionData, chartDiv, res.dataList)
                } else {
                    this.toastrService.error('NO Data Found!'); // Used to notify error responce
                }
            }, err => {
                this.toastrService.error(err); // Used to notify error responce
        });
    }

    createChartInstance(visualizationData, visualizationOptionData, chartDiv, chartData) {
//         switch (visualizationOptionData.selectedTheme) {
//             case "kelly":
// console.log(visualizationOptionData.selectedTheme)

//                 am4core.useTheme(kelly);
//                 break;

//             case "frozen":
// console.log(visualizationOptionData.selectedTheme)

//                 am4core.useTheme(frozen);
//                 break;

//             case "material":
// console.log(visualizationOptionData.selectedTheme)

//                 am4core.useTheme(material);
//                 break;

//             case "dataviz":
// console.log(visualizationOptionData.selectedTheme)
            
//                 am4core.useTheme(dataviz);
//                 break;

//             case "moonrisekingdom":
//                 am4core.useTheme(moonrisekingdom);
//                 break;

//             case "spiritedaway":
//                 am4core.useTheme(spiritedaway);
//                 break;

//             case "defaults":
//                 console.log('Deafult')
//                 am4core.useTheme(this.am4themes_cs);
//                 break;
//         }


        var chart:any;
        var pieSeries:any;
        var _self = this;
        // Create chart instance
        if (visualizationOptionData.PieChart3D) {
            chart = am4core.create(chartDiv, am4charts.PieChart3D);
            // Add and configure Series
            pieSeries = chart.series.push(new am4charts.PieSeries3D());
        } else {
            chart = am4core.create(chartDiv, am4charts.PieChart);
            // Add and configure Series
            pieSeries = chart.series.push(new am4charts.PieSeries());
        }
        
        pieSeries.dataFields.value = visualizationOptionData.selectedValue;
        pieSeries.dataFields.category = visualizationOptionData.selectedCategory;

        chart.innerRadius = am4core.percent(visualizationOptionData.innerRadius);
        chart.responsive.enabled = true;
        // Put a thick white border around each Slice
        if (visualizationOptionData.templateStroke) {
            pieSeries.slices.template.stroke = am4core.color("#fff");
        }

        pieSeries.slices.template.strokeWidth = 2;
        pieSeries.slices.template.strokeOpacity = 1;
        pieSeries.slices.template
            // change the cursor on hover to make it apparent the object can be interacted with
            .cursorOverStyle = [{
                "property": "cursor",
                "value": "pointer"
            }];

        pieSeries.alignLabels = false;
        const myColors= [
            "#fe6989",
            "#fd9428",
            "#fecf5b",
            "#50c4c3",
            "#FFC75F",
            "#F9F871"
          ];

          // pieSeries.slices.template.adapter.add("fill", (fill, target) => {
          //   return am4core.color(myColors[target.dataItem.index]);
          // });

        pieSeries.hiddenState.properties.endAngle = -90;
        if (visualizationOptionData.radiusValue) {
            pieSeries.dataFields.radiusValue = visualizationOptionData.selectedValue;
        }
        pieSeries.slices.template.cornerRadius = visualizationOptionData.cornerRadius;
        // pieSeries.ticks.template.disabled = true;
        // pieSeries.labels.template.text = "{value.percent.formatNumber('#.0')}%";
        // pieSeries.labels.template.radius = am4core.percent(-40);
        // pieSeries.labels.template.fill = am4core.color("white");
        // pieSeries.labels.template.relativeRotation = 90;
        pieSeries.labels.template.disabled = visualizationOptionData.labelsTemplateDisabled;
        // Create a base filter effect (as if it's not there) for the hover to return to
        var shadow = pieSeries.slices.template.filters.push(new am4core.DropShadowFilter);
        shadow.opacity = 0;

        // Create hover state
        var hoverState = pieSeries.slices.template.states.getKey("hover"); // normally we have to create the hover state, in this case it already exists

        // Slightly shift the shadow and make it more prominent on hover
        var hoverShadow = hoverState.filters.push(new am4core.DropShadowFilter);
        hoverShadow.opacity = 0.7;
        hoverShadow.blur = 5;

        if (visualizationOptionData.showAbsoluteValue) {
          pieSeries.slices.template.tooltipText = "{category}: {value.value}";
          pieSeries.labels.template.text = "{category}: {value.value}";
        }

        // Add a legend
        if (visualizationOptionData.legend) {
            chart.legend = new am4charts.Legend();
            chart.legend.position = "right";
            chart.legend.marginLeft = 0;
            chart.legend.marginRight = 0;
            chart.legend.labels.template.maxWidth = 50;
            chart.legend.width = 200;
            chart.legend.labels.template.truncate = false;
            let markerTemplate = chart.legend.markers.template;
                markerTemplate.width = 15;
                markerTemplate.height = 15;
        }

        if (visualizationData != undefined) {
          if (visualizationData.visualization_leaf.visualization_id != undefined) {
              pieSeries.slices.template.cursorOverStyle = am4core.MouseCursorStyle.pointer;
              pieSeries.slices.template.events.on("hit", function(ev) {
                  var dataContext:any
                  dataContext = ev.target.dataItem.category;
                  let query_obj = visualizationData.visualization_leaf.query_obj;
                  if (visualizationData.visualization_leaf.query_obj != undefined) {
                      query_obj[visualizationOptionData.selectedCategory] = dataContext;
                  }
              _self._router.navigate(['dashboard/leaf/' 
                             + visualizationData.visualization_id + '/' + _self.createStringUrl(query_obj)]);
              }, this);
          }
        }


        chart.data = chartData;
        return chart;
    }

  createStringUrl(obj) {
    var stringUrl = ""; 
      for (var key in obj) { 
          if (stringUrl != "") { 
              stringUrl += "&"; 
          } 
          stringUrl += (key + "=" + encodeURIComponent(obj[key])); 
      } 
    return stringUrl
  }
}


