import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { Config } from '../config/general.config';
import { API_URL } from '../config/env.config';

@Injectable()
export class ChartDataService {
  public apiRoute:string = 'mongodb-query';

  constructor(private _http: Http) { }
    getDataByCollectionNameWithDocumentFieldsNoPagination(data_source_id, collectionName, query, limit){
      let queryString = { perpage: limit, query: query};
      let queryParamsWithPaginationQueryAndAPIAccessToken:object = Config.queryParamsWithPaginationQueryAndAPIAccessToken(queryString)

      return  this._http.get(API_URL + 'mongodb-query/ByCollectionNameWithDocumentFieldsNoPagination/' + data_source_id + '/' + collectionName, 
        queryParamsWithPaginationQueryAndAPIAccessToken)
                .map(res => res.json())
                .catch(Config.handleError);
    }
  }