import { Component, OnInit, Input, OnDestroy, Renderer2, ChangeDetectorRef } from '@angular/core';
import { NavigationService } from "../../../shared/services/navigation.service";
import { Subscription } from 'rxjs';
import { ThemeService } from '../../../shared/services/theme.service';
import { TranslateService } from '@ngx-translate/core';
import { LayoutService } from '../../services/layout.service';
import { Router } from '@angular/router';

import { Config } from '../../services/config/general.config';
import { HeaderTopService } from './header-top.service';

import { SharedService } from '../../services/shared-service.service';

@Component({
  selector: 'app-header-top',
  templateUrl: './header-top.component.html'
})
export class HeaderTopComponent implements OnInit, OnDestroy {
  layoutConf: any;
  menuItems:any;
  menuItemSub: Subscription;
  egretThemes: any[] = [];
  currentLang = 'en';
  availableLangs = [{
    name: 'English',
    code: 'en',
  }, {
    name: 'Spanish',
    code: 'es',
  }]
  imgUrl:any = 'assets/images/default-user.png';

  asideVisible: boolean;
  user_role = Config.getUserRole();
  name;

  @Input() notificPanel;
  constructor(
    private layout: LayoutService,
    private navService: NavigationService,
    public themeService: ThemeService,
    public translate: TranslateService,
    private renderer: Renderer2,
    private _router: Router,
    public cdr: ChangeDetectorRef,
    public _objService: HeaderTopService,
    public _sharedService: SharedService
  ) { 
      this._sharedService.profilePictureChange.subscribe(value => {
          this.getUserDataByID();
      });
  }

  ngOnInit() {
    this.getUserDataByID();
    this.layoutConf = this.layout.layoutConf;
    this.egretThemes = this.themeService.egretThemes;
    // this.updateMenuByRoles()
    // this.menuItemSub = this.navService.menuItems$
    // .subscribe(res => {
    //   res = res.filter(item => item.type !== 'icon' && item.type !== 'separator');
    //   let limit = 6
    //   let mainItems:any[] = res.slice(0, limit)
    //   if(res.length <= limit) {
    //     // return this.menuItems = mainItems
    //         console.log(this.updateMenuByRoles(mainItems));

    //     return this.updateMenuByRoles(mainItems);

    //   }
    //   let subItems:any[] = res.slice(limit, res.length - 1)
    //   mainItems.push({
    //     name: 'More',
    //     type: 'dropDown',
    //     tooltip: 'More',
    //     icon: 'more_horiz',
    //     sub: subItems
    //   })
    //   this.menuItems = mainItems
    // })
  }
  ngOnDestroy() {
    this.menuItemSub.unsubscribe()
  }

//--------------------------------------------//

  getUserDataByID() {
    this._objService
      .getUserDataByID(Config.getUserId())
      .subscribe(userData => {
        this.name = userData.usr_name;
        if (userData.usr_profile_pic != undefined) {
          if (userData.usr_profile_pic.length != 0) {
            this.imgUrl = userData.usr_profile_pic[0].imgUrl
          }
          this.cdr.detectChanges();
        }
        if (userData.usr_role == 'admin' || userData.usr_role == 'supadmin') {
          this.menuItemSub = this.navService.menuItems$
                  .subscribe(res => {
                    res = res.filter(item => item.type !== 'icon' && item.type !== 'separator');
                    let limit = 6
                    let mainItems:any[] = res.slice(0, limit)
                    if(res.length <= limit) {
                        return this.menuItems = mainItems
                    }
                    let subItems:any[] = res.slice(limit, res.length - 1)
                    mainItems.push({
                      name: 'More',
                      type: 'dropDown',
                      tooltip: 'More',
                      icon: 'more_horiz',
                      sub: subItems
                    })
                    this.menuItems = mainItems;
                  })
        } else {
          this._objService.getRoleDataByOrgIdAndRoleName(userData.org_id, userData.usr_role).subscribe(roleData => {
              this.menuItemSub = this.navService.menuItems$
                  .subscribe(res => {
                    res = res.filter(item => item.type !== 'icon' && item.type !== 'separator');
                    let limit = 8
                    let mainItems:any[] = res.slice(0, limit)
                    if(res.length <= limit) {
                      // console.log(this.updateMenuByRoles(mainItems, roleData))
                      this.cdr.detectChanges();
                      return this.removeSubZeroMenu(this.updateMenuByRoles(mainItems, roleData));
                    }
                    let subItems:any[] = res.slice(limit, res.length - 1)
                    mainItems.push({
                      name: 'More',
                      type: 'dropDown',
                      tooltip: 'More',
                      icon: 'more_horiz',
                      sub: subItems
                    })
                    this.menuItems = mainItems;
                    this.cdr.detectChanges();
                  })
                    this.cdr.detectChanges();

            }, err => {
          });
        }
    }, error => {
    });
  }

  removeSubZeroMenu(mainItems) {
    const menus = [];
    mainItems.forEach((v, i) => {
        menus.push(mainItems[i]);
    });
    for (var i = 0; i < mainItems.length;) {
      if (i >= (mainItems.length - 1)) {
          return this.menuItems = menus;
          break;
      } else {
        if (menus[i].sub != undefined ) {
             if (menus[i].sub.length == 0) {
               menus.splice(i, 1);
               i++;
             } else {
               i++;
             }
          } else {
            i++;
        }
      }
    }
    return this.menuItems = mainItems
  }


  updateMenuByRoles(mainItems, roleData) {
    let components = roleData.components;
    const menus = [];
    for (var i = 0; i < mainItems.length;) {
    if (mainItems[i].sub != undefined) {
      var sub = [];
      for (var j = 0; j < mainItems[i].sub.length;) {
        if (mainItems[i].sub[j].component != undefined) {
          var roleInfo = components.find(item => item.component_name === mainItems[i].sub[j].component);
            if (roleInfo != undefined) {
              switch (mainItems[i].sub[j].method){
                case "GET":
                  if(roleInfo.read){
                    sub.push(mainItems[i].sub[j]);
                  }
                  j++
                  break;
                case "POST":
                  if(roleInfo.write && roleInfo.create){
                    sub.push(mainItems[i].sub[j]);
                  }
                  j++
                  break;
                case "PUT":
                case "PATCH":
                  if(roleInfo.write && roleInfo.change){
                    sub.push(mainItems[i].sub[j]);
                  }
                  j++
                  break;
                case "DELETE":
                  if(roleInfo.delete) {
                    sub.push(mainItems[i].sub[j]);
                  }
                  j++
                  break;
          }
        } else {
          j++;
        }
       } 

       if (j >= (mainItems[i].sub.length)) {

          let menuData = {
              name: mainItems[i].name,
              type: mainItems[i].type,
              tooltip: mainItems[i].tooltip,
              icon: mainItems[i].icon,
              state: mainItems[i].state,
              sub: sub
          }
          menus.push(menuData);
          i++;
          break;
        } 


     }

    } else {
        if (mainItems[i].component != undefined) {
        var roleInfo = components.find(item => item.component_name === mainItems[i].component);
          if (roleInfo != undefined) {

          switch (mainItems[i].method){
              case "GET":
                  if(roleInfo.read){
                    menus.push(mainItems[i]);
                  }
                  i++;
                  break;
              case "POST":
                  if(roleInfo.write && roleInfo.create){
                    menus.push(mainItems[i]);
                  }
                  i++;
                  break;
              case "PUT":
              case "PATCH":
                  if(roleInfo.write && roleInfo.change){
                    menus.push(mainItems[i]);
                  }
                  i++;
                  break;
              case "DELETE":
                  if(roleInfo.delete) {
                    menus.push(mainItems[i]);
                  }
                  i++;
                  break;
            }
          }
      } else {
        menus.push(mainItems[i]);
        i++;
      }
    }
      if (i >= (mainItems.length)) {
        return menus;
        break;
      } 
    }
  }


  checkSubMenuAccess(mainItemList) {
    let menuWithSub = []
    for (var i = 0; i < mainItemList.length; i++) {
      if (mainItemList[i].sub != undefined) {
        for (var j = 0; j < mainItemList[i].sub.length; j++) {

          // for (var j = 0; j < mainItemList[i].sub[j].access.length; j++) {
            if (!mainItemList[i].sub[j].access.includes(Config.getUserRole()) ) {
              mainItemList[i].sub.splice(j, 1);
            }
          // } 

        }
      } 
    }

    return this.menuItems = mainItemList;
  }

remove(array, key, value) {
    const index = array.findIndex(obj => obj[key] === value);
    return index >= 0 ? [
        ...array.slice(0, index),
        ...array.slice(index + 1)
    ] : array;
}

// //--------------------------------------------//
// /**
// * Calling getUserDataByID function from profileService to get the User profile data from database
// */
//   getUserDataByID() {
//     this._objService
//       .getUserDataByID(Config.getUserId())
//       .subscribe(res => {
//         this.name = res.usr_name;
//         if (res.usr_profile_pic != undefined) {
//           if (res.usr_profile_pic.length != 0) {
//             this.imgUrl = res.usr_profile_pic[0].imgUrl
//           }
//           this.cdr.detectChanges();
//         }
//     }, error => {
//          // this.errorMessage(error); // Used to notify error responce.
//     });
//   }

  setLang() {
    this.translate.use(this.currentLang)
  }
  changeTheme(theme) {
    this.layout.publishLayoutChange({matTheme: theme.name})
  }
  toggleNotific() {
    this.notificPanel.toggle();
  }
  toggleSidenav() {
    if(this.layoutConf.sidebarStyle === 'closed') {
      return this.layout.publishLayoutChange({
        sidebarStyle: 'full'
      })
    }
    this.layout.publishLayoutChange({
      sidebarStyle: 'closed'
    })
  }

    // Function to goToProfile
    goToProfile() {
      this._router.navigate(['profile/', Config.getUserId()]); // Navigate to profile page
    }
    //Function to gotoProfilesetting
    goToAccountSettings() {
      this._router.navigate(['profile-settings/']); // Navigate to profile page
    }
    //Function to goToDashboard
    goToDashboard(){
      this._router.navigate(['/dashboard/default']); // Navigate to dashboard page
    }
  
    // Function to logout user
    logout() {
      Config.clearToken(); // Logout user
      this._router.navigate(['/account/signin']); // Navigate back to home page
    }
}
