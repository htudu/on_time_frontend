import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// SERVICES
import { ThemeService } from './services/theme.service';
import { NavigationService } from "./services/navigation.service";
import { RoutePartsService } from './services/route-parts.service';
import { AuthGuard } from './services/auth/auth.guard';
import { RoleGuard } from './services/auth/role.guard';
import { BlockRoleGuard } from './services/auth/block-role.guard';
import { HasOnlyRoleGuard } from './services/auth/has-only-role.guard';
import { NotAuthGuard } from './services/auth/notAuth.guard';
import { AppConfirmService } from './services/app-confirm/app-confirm.service';
import { AppLoaderService } from './services/app-loader/app-loader.service';

import { SharedComponentsModule } from './components/shared-components.module';
import { SharedPipesModule } from './pipes/shared-pipes.module';
import { SharedDirectivesModule } from './directives/shared-directives.module';

import { SharedService } from './services/shared-service.service';
import { ChartDataService } from './services/charts/chart-data.service';

import { PieChartService } from './services/charts/am-charts/pie-chart.service';
import { LineChartService } from './services/charts/am-charts/LineChart.service';
import { PieChart3DService } from './services/charts/am-charts/PieChart3D.service';
import { StackedColumnChartService } from './services/charts/am-charts/stacked-column-chart.service';
import { RadarChartService } from './services/charts/am-charts/RadarChart.service';
import { XYChartService } from './services/charts/am-charts/XYChart.service';
import { XYChart3DService } from './services/charts/am-charts/XYChart3D.service';
import { GaugeChartService } from './services/charts/am-charts/GaugeChart.service';
import { BubbleChartService } from './services/charts/am-charts/BubbleChart.service';
import { ZoomableRadarChartService } from './services/charts/am-charts/ZoomableRadarChart.service';
import { StackedAreaRadarService } from './services/charts/am-charts/StackedAreaRadar.service';
import { MapChartService } from './services/charts/am-charts/MapChart.service';
import { MapCountriesService } from './services/charts/am-charts/MapCountries.service';

import { ForceDirectedNetworkChartService } from './services/charts/am-charts/ForceDirectedNetwork.service';
import { CollapsibleForceDirectedTreeService } from './services/charts/am-charts/CollapsibleForceDirectedTree.service';
import { SolidGaugeService } from './services/charts/am-charts/SolidGauge.service';

import { CalendarHeatmapService } from './services/charts/echarts/calendar-heatmap.service';

import { WidgetService } from './services/charts/custom/widget.service';
import { DataTableService } from './services/charts/custom/data-table.service';


import { AuthService } from './services/auth/auth.service';

@NgModule({
  imports: [
    CommonModule,
    SharedComponentsModule,
    SharedPipesModule,
    SharedDirectivesModule    
  ],
  providers: [
    ThemeService,
    NavigationService,
    RoutePartsService,
    AuthGuard,
    RoleGuard,
    BlockRoleGuard,
    HasOnlyRoleGuard,
    NotAuthGuard,
    AppConfirmService,
    AppLoaderService,
    
    SharedService,

    ChartDataService,
    
    PieChartService,
    LineChartService,
    PieChart3DService,

    StackedColumnChartService,
    RadarChartService,
    XYChartService,
    XYChart3DService,
    GaugeChartService,
    BubbleChartService,
    ZoomableRadarChartService,
    StackedAreaRadarService,
    MapChartService,
    MapCountriesService,
    ForceDirectedNetworkChartService,
    CollapsibleForceDirectedTreeService,
    SolidGaugeService,

    CalendarHeatmapService,

    WidgetService,
    DataTableService,

    AuthService
  ],
  exports: [
    SharedComponentsModule,
    SharedPipesModule,
    SharedDirectivesModule
  ]
})
export class SharedModule { }
